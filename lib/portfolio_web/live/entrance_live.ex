defmodule PortfolioWeb.EntranceLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
      <div class="flex justify-center bg-[url('/images/new/bg_reg.svg')] bg-cover bg-center bg-center">
        <div class="pb-10">
          <div class="flex flex-col content-center bg-white w-auto h-auto rounded-md my-20 px-8 pt-10 pb-8 gap-5 z-20">
            <div class="quicksand font-bold text-2xl">Войти</div>
              <div>
                <input type="email" class="rounded-md w-full" placeholder="Email">
              </div>
              <input type="password" class="rounded-md w-full" placeholder="Пароль">
              <div class="flex pt-4">
                  <button class="relative flex items-center justify-center py-4 rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] font-bold text-white w-full uppercase">Войти<span style="height: calc(100% - 2px); top:1px; right:1px;" class="text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-white"></i></span></button>
              </div>
              <div class="flex justify-between items-center pt-4 gap-8">
                <div class="flex">Нет аккаунта на Toxin?</div>
                <div class="bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] rounded-full p-0.5">
                  <button class="flex items-center rounded-full border-none bg-white px-4 py-1 font-bold text-[#BC9CFF]"><a href="/reg">СОЗДАТЬ</a></button>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="bg-white mt-10 px-40 pt-10">
        <div class="space-y-10 md:grid md:grid-cols-5 md:gap-x-8 md:gap-y-10 md:space-y-0 pb-20">
          <div class="flex flex-col gap-6">
            <div class="flex content-center">
              <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
            </div>
            <div class="montserrat font-medium text-[#1f204180]">Бронирование номеров в лучшем отеле 2019 года по версии ассоциации «Отельные взгляды»</div>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Навигация</a></li>
              <li><a href="">О нас</a></li>
              <li><a href="">Новости</a></li>
              <li><a href="">Служба поддержки</a></li>
              <li><a href="">Услуги</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">О нас</a></li>
              <li><a href="">О сервисе</a></li>
              <li><a href="">Наша команда</a></li>
              <li><a href="">Вакансии</a></li>
              <li><a href="">Инвесторы</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Служба поддержки</a></li>
              <li><a href="">Соглашения</a></li>
              <li><a href="">Сообщества</a></li>
              <li><a href="">Связь с нами</a></li>
            </ul>
          </div>
          <div class="flex flex-col justify-start gap-6 montserrat font-normal text-[#1f204180] justify-start">
            <div class="font-bold uppercase text-[#1f2041bf]">Подписка</div>
            <div class="montserrat font-medium text-[#1f204180]">Получайте специальные предложения и новости сервиса</div>
            <div class="montserrat font-medium text-[#8BA4F9]">
              <div class="relative w-full ">
                <input type="email" class=" w-full rounded-md border-[#1f204140] text-[#8BA4F9]" placeholder="Email" />
                <span style="height: calc(100% - 2px); top:1px; right:1px;" class="bg-white text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-[#8BA4F9]"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="flex justify-between justify-items-center items-center h-16 border-t-2 border-[#1f20410d]">
          <div class="flex montserrat font-medium text-[#1f204180]">
          Copyright © 2018 Toxin UI Kit. All rights reserved.
          </div>
          <div class="flex gap-4 text-3xl text-[#8BA4F9]">
            <div><i class="ri-twitter-fill"></i></div>
            <div><i class="ri-facebook-box-fill"></i></div>
            <div><i class="ri-instagram-line"></i></div>
          </div>
        </div>
      </div>

    <% end %>
    """
  end

    # <div class="welcome-text">
    #   Добро пожаловать на мою страницу
    # </div>
end
