defmodule PortfolioWeb.ContactsLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def component_1(assigns) do
    ~H"""
    <div class="bg-white py-12">
        <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
          <div class="lg:text-center">
            <h2 class="text-lg font-semibold text-indigo-600">Transactions</h2>
            <p class="mt-2 text-3xl font-bold leading-8 tracking-tight text-gray-900 sm:text-4xl">A better way to send money</p>
            <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">Lorem ipsum dolor sit amet consect adipisicing elit. Possimus magnam voluptatum cupiditate veritatis in accusamus quisquam.</p>
          </div>

          <div class="mt-10">
            <dl class="space-y-10 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10 md:space-y-0">
              <div class="relative">
                <dt>
                  <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                    <i class="ri-global-line text-2xl"></i>
                  </div>
                  <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Competitive exchange rates</p>
                </dt>
                <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</dd>
              </div>

              <div class="relative">
                <dt>
                  <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                    <i class="ri-scales-3-line text-2xl"></i>
                  </div>
                  <p class="ml-16 text-lg font-medium leading-6 text-gray-900">No hidden fees</p>
                </dt>
                <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</dd>
              </div>

              <div class="relative">
                <dt>
                  <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                    <i class="ri-flashlight-line text-2xl"></i>
                  </div>
                  <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Transfers are instant</p>
                </dt>
                <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</dd>
              </div>

              <div class="relative">
                <dt>
                  <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                    <i class="ri-chat-check-line text-2xl"></i>
                  </div>
                  <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Mobile notifications</p>
                </dt>
                <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
    """
  end
  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
    <%= component_1(assigns) %>
    <.component_1 id={1}/>

      <div class="bg-gray-50 rounded-md py-12">
        <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
          <div class="lg:text-center">
            <h2 class="flex flex-col text-lg font-semibold text-indigo-600">
              <span class="mt-2 text-3xl font-bold leading-8 tracking-tight text-gray-900 sm:text-4xl">A better way to send money</span>
              <span class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">Lorem ipsum dolor sit amet consect adipisicing elit. Possimus magnam voluptatum cupiditate veritatis in accusamus quisquam.</span>
            </h2>
          </div>

          <div class="pt-10">
            <dl class="space-y-10 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10 md:space-y-0">

              <div class="flex flex-col gap-8">
                <div>
                  <h2 class="flex flex-col text-lg font-semibold text-indigo-600">
                    <span class="mt-2 text-2xl font-bold leading-8 tracking-tight text-gray-900 sm:text-4xl">Transfer funds world-wide</span>
                    <span class="mt-4 max-w-2xl text-xl text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!</span>
                  </h2>
                </div>
                <div class="relative mt-8">
                  <dt>
                    <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                      <i class="ri-global-line text-2xl"></i>
                    </div>
                    <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Competitive exchange rates</p>
                  </dt>
                  <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!
                  </dd>
                </div>

                <div class="relative">
                  <dt>
                    <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                      <i class="ri-scales-3-line text-2xl"></i>
                    </div>
                    <p class="ml-16 text-lg font-medium leading-6 text-gray-900">No hidden fees</p>
                  </dt>
                  <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!
                  </dd>
                </div>

                <div class="relative">
                  <dt>
                    <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                      <i class="ri-flashlight-line text-2xl"></i>
                    </div>
                    <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Transfers are instant</p>
                  </dt>
                  <dd class="mt-2 ml-16 text-base text-gray-500 ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!
                  </dd>
                </div>
              </div>

              <div class="p-6 flex justify-end">
                <div class="bg-white rounded-md drop-shadow-md">
                  <div class="py-3 px-6">
                    <h3 class="flex flex-col text-lg font-semibold text-indigo-600">
                      <span class="mt-4 text-3xl  leading-8 tracking-tight text-gray-900 sm:text-xl">Send money</span>
                      <span class="text-base font-normal text-gray-500 start"> Lorem ipsum dolor sit amet consect adipisicing elit.</span>
                    </h3>
                  </div>
                  <div class="flex flex-col py-3 px-6 gap-2">
                    <label class="flex font-medium">Recipient</label>
                    <select class="w-full flex justify-center rounded-md">
                      <option>Мицкевич Дмитрий</option>
                      <option>Мицкевич Ольга</option>
                      <option>Шлыков Сергей</option>
                      <option>Буглов Вячеслав</option>
                    </select>
                  </div>

                  <div class="h-px w-full bg-gray-200 mt-4">
                  </div>

                  <div>
                    <div class="flex flex-col py-3 px-6 gap-2 mt-4">
                      <label class="flex font-medium">How much do you want to send</label>
                      <div class="w-full flex">
                        <input type="text" class="w-4/5 rounded-l-md" placeholder="0" />
                        <select class="rounded-r-md w-auto bg-gray-50">
                          <option>RUB</option>
                          <option>EUR</option>
                          <option>USD</option>
                          <option>CHF</option>
                        </select>
                      </div>
                      <div class="mt-4">
                        <div class="relative w-full">
                          <input type="text" class=" w-full rounded-t-md" placeholder="0" />
                          <span style="height: calc(100% - 2px); top:1px; right:1px;" class="bg-white text-base text-gray-500 absolute border-l border-gray-600 flex justify-center items-center px-4 rounded-r-md">Привет</span>
                        </div>
                        <input type="text" class="w-full rounded-b-md" placeholder="0"/>
                      </div>
                      <label class="flex font-medium mt-4">Recipient gets</label>
                      <div class="w-full flex">
                        <input type="text" class="w-4/5 rounded-l-md" placeholder="0" />
                        <select class="rounded-r-md w-auto bg-gray-50 ">
                          <option>RUB</option>
                          <option>EUR</option>
                          <option>USD</option>
                          <option>CHF</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="flex justify-end w-full bg-gray-50 border-gray-200 mt-4 rounded-b-md py-4 px-6">
                    <div class="flex justify-between">
                      <button class="flex justify-center items-center py-2 px-4">Cancel</button>
                      <button class="flex justify-center items-center rounded-md bg-indigo-400 py-2 px-4 text-gray-50 hover:bg-indigo-500 transition_up">Continue</button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="flex flex-col gap-2 max-w-md justify-center content-center">
                <div class="flex items-center justify-between bg-white rounded-md drop-shadow-xl py-2 px-4 gap-4">
                  <div class="flex items-center rounded-full w-auto bg-indigo-400 ">
                    <i class="ri-money-dollar-circle-line py-4 px-5 text-white text-xl"></i>
                  </div>
                  <div class="flex flex-col w-2/3">
                    <div>Payment to Courtland Fenning</div>
                    <div class="font-bold">$20,000 <span class="font-normal">USD</span></div>
                  </div>
                  <div class="flex">
                    <div class="flex flex-row items-center bg-green-100 rounded-full px-2 py-1 gap-2">
                      <div class="items-center bg-green-500 rounded-full w-2 h-2">
                      </div>
                      <div class="flex text-emerald-500 font-medium">
                        Success
                      </div>
                    </div>
                  </div>
                </div>

                <div class="flex items-center justify-between bg-white rounded-md drop-shadow-xl py-2 px-4 gap-4">
                  <div class="flex items-center rounded-full w-auto bg-indigo-400">
                    <i class="ri-money-dollar-circle-line py-4 px-5 text-white text-xl"></i>
                  </div>
                  <div class="flex flex-col w-2/3">
                    <div>Payment to Molly Sanders</div>
                    <div class="font-bold">$4,500 <span class="font-normal">USD</span></div>
                  </div>
                  <div class="flex">
                    <div class="flex flex-row items-center bg-orange-200 rounded-full px-2 py-1 gap-2">
                      <div class="items-center bg-orange-500 rounded-full w-2 h-2">
                      </div>
                      <div class="flex text-amber-500 font-medium">
                        Processing
                      </div>
                    </div>
                  </div>
                </div>

                <div class="flex items-center justify-between bg-white rounded-md drop-shadow-xl py-2 px-4 gap-4">
                  <div class="flex items-center rounded-full w-auto bg-indigo-400 ">
                    <i class="ri-money-dollar-circle-line py-4 px-5 text-white text-xl"></i>
                  </div>
                  <div class="flex flex-col w-2/3">
                    <div>Payment to Tom Cook</div>
                    <div class="font-bold">$14,500 <span class="font-normal">USD</span></div>
                  </div>
                  <div class="flex">
                    <div class="flex flex-row items-center bg-green-100 rounded-full px-2 py-1 gap-2">
                      <div class="items-center bg-green-500 rounded-full w-2 h-2">
                      </div>
                      <div class="flex text-emerald-500 font-medium">
                        Success
                      </div>
                    </div>
                  </div>
                </div>

                <div class="flex items-center justify-between bg-white rounded-md drop-shadow-xl py-2 px-4 gap-4">
                  <div class="flex items-center rounded-full w-auto bg-indigo-400 ">
                    <i class="ri-money-dollar-circle-line py-4 px-5 text-white text-xl"></i>
                  </div>
                  <div class="flex flex-col w-2/3">
                    <div>Payment to Rebecca Heart</div>
                    <div class="font-bold">$9,500 <span class="font-normal">USD</span></div>
                  </div>
                  <div class="flex">
                    <div class="flex flex-row items-center bg-red-100 rounded-full px-2 py-1 gap-2">
                      <div class="items-center bg-red-500 rounded-full w-2 h-2">
                      </div>
                      <div class="flex text-orange-500 font-medium">
                        Failed
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="flex flex-col gap-8 justify-center content-center">
                <div>
                  <h2 class="flex flex-col text-lg font-semibold text-indigo-600">
                    <span class="mt-2 text-2xl font-bold leading-8 tracking-tight text-gray-900 sm:text-4xl">Always in the loop</span>
                    <span class="mt-4 max-w-2xl text-xl text-gray-500">Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!</span>
                  </h2>
                </div>
                <div class="relative mt-8">
                  <dt>
                    <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                      <i class="ri-chat-check-line text-2xl"></i>
                    </div>
                    <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Mobile notifications</p>
                  </dt>
                  <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!
                  </dd>
                </div>

                <div class="relative">
                  <dt>
                    <div class="absolute flex h-12 w-12 items-center justify-center rounded-md bg-indigo-500 text-white">
                      <i class="ri-mail-line text-2xl"></i>
                    </div>
                    <p class="ml-16 text-lg font-medium leading-6 text-gray-900">Remainder emails</p>
                  </dt>
                  <dd class="mt-2 ml-16 text-base text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores magni non ab dolor ducimus eius enim, excepturi reiciendis itaque perferendis! Repellendus minus quae autem fugiat tempora pariatur fuga eligendi vel!
                  </dd>
                </div>

              </div>

            </dl>
          </div>

        </div>
      </div>
    <% end %>
    """
  end

end
