defmodule PortfolioWeb.SkillsLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
      <div class="w-full max-w-full h-auto bg-white px-40 py-2">
        <div class="flex flex-row justify-between items-center">
          <div class="flex content-center justify-center">
            <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
          </div>
          <div class="flex justify-end gap-8 text-sm">
            <ul class= "flex justify-between items-center gap-6 montserrat font-normal text-[#1f204180] topmenu">
              <li class="font-bold text-[#1f2041bf]"><a href="">Home</a></li>
              <li><a href="">About Us</a></li>
              <li class="flex flex-row justify-center content-center"><a class="flex items-center" href="">Services<i class="flex ri-arrow-down-s-line font-normal py-4"></i></a></li>
                <ul class="hidden">
                  <li class=":hover block"><a href="">Item 1</a></li>
                  <li class=":hover block"><a href="">Item 2</a></li>
                  <li class=":hover block"><a href="">Item 3</a></li>
                </ul>
              <li><a href="">Careers</a></li>
              <li><a href="">News</a></li>
              <li><a href="">Documentation</a></li>
            </ul>
            <div class="flex flex-row justify-around content-center gap-4 py-2">
              <button class="flex items-center rounded-full border-2 border-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] px-4 py-1 font-bold text-[#D9D9D9]">LOGIN</button>
              <button class="flex items-center rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] px-4 py-1 font-bold text-white drop-shadow-lg">REGISTER</button>
            </div>
          </div>
        </div>
      </div>

      <div class="mt-10 w-full max-w-full h-auto bg-white px-40 py-2">
        <div class="flex flex-row justify-between content-center">
          <div class="flex items-center justify-center">
            <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
          </div>
          <div class="flex justify-end items-center gap-8 text-sm">
            <ul class= "flex justify-between items-center gap-6 montserrat font-normal text-[#1f204180] topmenu">
              <li class="font-bold text-[#1f2041bf]"><a href="">Home</a></li>
              <li><a href="">About Us</a></li>
              <li class="flex flex-row justify-center content-center"><a class="flex items-center" href="">Services<i class="flex ri-arrow-down-s-line font-normal py-4"></i></a></li>
                <ul class="hidden :hover block">
                  <li class=":hover block"><a href="">Item 1</a></li>
                  <li class=":hover block"><a href="">Item 2</a></li>
                  <li class=":hover block"><a href="">Item 3</a></li>
                </ul>
              <li><a href="">Careers</a></li>
              <li><a href="">News</a></li>
              <li><a href="">Documentation</a></li>
            </ul>
            <div class="flex w-0.5 h-6 bg-[#1f20411a]">
            </div>
            <div class="flex justify-between items-center montserrat font-normal text-[#1f204180]">
            Cosmin Negoita
            </div>
          </div>
        </div>
      </div>

      <div class="bg-white mt-10 px-40 pt-10">
        <div class="space-y-10 md:grid md:grid-cols-5 md:gap-x-8 md:gap-y-10 md:space-y-0 pb-20">
          <div class="flex flex-col gap-6">
            <div class="flex content-center">
              <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
            </div>
            <div class="montserrat font-medium text-[#1f204180]">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore aspernatur perferendis delectus explicabo id dolor vitae maxime possimus.</div>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180]">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Navigation</a></li>
              <li><a href="">About Us</a></li>
              <li><a href="">News</a></li>
              <li><a href="">Support</a></li>
              <li><a href="">Products</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180]">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">About Us</a></li>
              <li><a href="">Who we are</a></li>
              <li><a href="">Our team</a></li>
              <li><a href="">Careers</a></li>
              <li><a href="">Investors</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180]">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Support</a></li>
              <li><a href="">Documentation</a></li>
              <li><a href="">Community</a></li>
              <li><a href="">Get in Touch</a></li>
            </ul>
          </div>
          <div class="flex flex-col justify-start gap-6 montserrat font-normal text-[#1f204180] justify-start">
            <div class="font-bold uppercase text-[#1f2041bf]">Subscribe to our newsletter</div>
            <div class="montserrat font-medium text-[#1f204180]">Receive our latest news and promotions in your inbox!</div>
            <div class="montserrat font-medium text-[#8BA4F9]">
              <div class="relative w-full ">
                <input type="email" class=" w-full rounded-md border-[#1f204140] text-[#8BA4F9]" placeholder="Your email adress" />
                <span style="height: calc(100% - 2px); top:1px; right:1px;" class="bg-white text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-[#8BA4F9]"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="flex justify-between justify-items-center items-center h-16 border-t-2 border-[#1f20410d]">
          <div class="flex montserrat font-medium text-[#1f204180]">
          Copyright © 2018 Toxin UI Kit. All rights reserved.
          </div>
          <div class="flex gap-4 text-3xl text-[#8BA4F9]">
            <div><i class="ri-twitter-fill"></i></div>
            <div><i class="ri-facebook-box-fill"></i></div>
            <div><i class="ri-instagram-line"></i></div>
          </div>
        </div>
      </div>

      <div class="pt-10">
        <div class="flex content-center h-96 bg-white space-y-10 md:grid md:gap-x-8 md:gap-y-10 md:space-y-0">
          <div class="flex flex-col justify-center content-center gap-4">
            <div class="flex justify-center content-center">
              <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
            </div>
            <div class="flex flex-col justify-center content-center montserrat font-medium text-[#1f204180]">
              <span class="flex justify-center">Lorem ipsum dolor sit amet consectetur adipiscing elit aliquam eget nullam</span>
              <span class="flex justify-center">pellentesque aliquam curabitur cociis.</span>
            </div>
          </div>
          <div class="flex justify-center items-center gap-4 text-3xl text-[#8BA4F9]">
            <div><a href=""><i class="ri-twitter-fill"></i></a></div>
            <div><a href=""><i class="ri-facebook-box-fill"></i></a></div>
            <div><a href=""><i class="ri-instagram-line"></i></a></div>
          </div>
        </div>
      </div>

      <div class="mt-10 px-40 pt-10 bg-black">
        <div class="space-y-10 md:grid md:grid-cols-3 space-y-10 md:grid md:gap-x-8 md:gap-y-10 md:space-y-0">

          <div class="flex flex-col gap-6 h-min items-center">
            <div class="flex flex-col content-center bg-white w-5/6 h-min rounded-md px-8 pt-10 pb-8 gap-5">
              <h1 class="quicksand font-bold text-2xl">Найдём номера под
                <span class="flex">ваши пожелания</span>
              </h1>
              <div class="flex flex-row justify-center gap-4 w-full">
                <div class="flex flex-col basis-1/2">
                  <label class="uppercase montserrat font-bold text-xs" for="start">Прибытие</label>
                  <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
                </div>

                <div class="flex flex-col basis-1/2">
                  <label class="uppercase montserrat font-bold text-xs" for="start">Выезд</label>
                  <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
                </div>
              </div>

              <div class="flex flex-col">
                <label class="uppercase montserrat font-bold text-xs" for="start">Гости</label>
                <input class="rounded-md w-full" type="number" id="start" name="trip-start" placeholder="Сколько гостей" min="0" max="10">
              </div>

              <div class="flex pt-4">
                <button class="relative flex items-center justify-center py-4 rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] font-bold text-white w-full uppercase">Подобрать номер<span style="height: calc(100% - 2px); top:1px; right:1px;" class="text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-white"></i></span></button>
              </div>
            </div>

            <div class="flex flex-col content-center bg-white w-5/6 rounded-md px-8 pt-10 pb-8 gap-5">
              <div class="quicksand font-bold text-2xl">Регистрация аккаунта</div>
              <div class="flex flex-col gap-4">
                <input type="text" class="rounded-md w-full" placeholder="Имя">
                <input type="text" class="rounded-md w-full" placeholder="Фамилия">
                <p class="flex gap-4">
                  <label class="flex gap-1 items-center">
                    <input class="" type="radio" name="title" value="Мужчина"> Мужчина
                  </label>
                  <label class="flex gap-1 items-center">
                    <input class="" type="radio" name="title" value="Женщина"> Женщина
                  </label>
                </p>
              </div>
              <div class="flex flex-col basis-1/2">
                <label class="uppercase montserrat font-bold text-xs" for="">Дата рождения</label>
                <input class="rounded-md w-full" type="date" id="" name="trip-start" value="ГГГГ-ММ-ДД" min="1930-01-01" max="2022-12-31">
              </div>

              <div class="flex flex-col gap-4">
                <div>
                  <label class="uppercase montserrat font-bold text-xs" for="">Данные для входа в сервис</label>
                  <input type="email" class="rounded-md w-full" placeholder="Email">
                </div>
                <input type="password" class="rounded-md w-full" placeholder="Пароль">
                <p class="flex items-center gap-4">
                  <label class="flex gap-2 items-center">
                    <input class="flex justify-center rounded-full" type="checkbox" name="title">Получать спецпредложения
                  </label>
                </p>

                <div class="flex pt-4">
                  <button class="relative flex items-center justify-center py-4 rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] font-bold text-white w-full uppercase">Перейти к оплате<span style="height: calc(100% - 2px); top:1px; right:1px;" class="text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-white"></i></span></button>
                </div>

                <div class="flex justify-between items-center pt-4">
                  <div class="flex">Уже есть аккаунт на Toxin
                  </div>
                  <div class="bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] rounded-full p-0.5">
                    <button class="flex items-center rounded-full border-none bg-white px-4 py-1 font-bold text-[#BC9CFF]">Войти</button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="flex flex-col gap-6 h-min items-center">

            <div class="flex flex-col content-center bg-white w-5/6 rounded-md px-8 pt-10 pb-8 gap-5 h-min">
              <div class="flex justify-between items-end">
                <div class="flex items-end gap-1">
                  <span class="quicksand font-bold">№</span>
                  <span class="quicksand font-bold text-2xl">888</span>
                  <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                </div>
                <div class="flex montserrat gap-1">
                  <span class="font-bold text-slate-500">9 990₽</span>
                  <span class="font-normal text-slate-500">в сутки</span>
                </div>
              </div>

              <div class="flex flex-row justify-center gap-4 w-full">
                <div class="flex flex-col basis-1/2">
                  <label class="uppercase montserrat font-bold text-xs" for="start">Прибытие</label>
                  <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
                </div>

                <div class="flex flex-col basis-1/2">
                  <label class="uppercase montserrat font-bold text-xs" for="start">Выезд</label>
                  <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
                </div>
              </div>

              <div class="flex flex-col">
                <label class="uppercase montserrat font-bold text-xs" for="start">Гости</label>
                <input class="rounded-md w-full" type="number" id="start" name="trip-start" placeholder="Сколько гостей" min="0" max="10">
              </div>

              <div class="montserrat font-normal text-slate-500 flex flex-col gap-2.5">
                <div class="flex justify-between">
                  <span>9 990₽ х 4 суток</span>
                  <span>39 960₽</span>
                </div>
                <div class="flex justify-between ">
                  <span class="flex items-center gap-1 w-4/5">Сбор за услуги: скидка 2 179₽ <i class="ri-information-line text-lg font-bold"></i></span>
                  <span>0₽</span>
                </div>
                <div class="flex justify-between">
                  <span class="flex justify-between items-start gap-1 w-2/3">Сбор за дополнительные услуги <i class="ri-information-line text-lg font-bold"></i></span>
                  <span>300₽</span>
                </div>
              </div>

              <div class="flex justify-between items-end">
                <div class="quicksand font-bold text-2xl">Итого</div>
                <div class="flex bg-slate-500 w-full h-px mb-1.5"></div>
                <div class="flex justify-around gap-1 items-end quicksand font-bold text-2xl w-auto">38 <span class="flex">081₽</span></div>
              </div>

              <div class="flex pt-4">
                  <button class="relative flex items-center justify-center py-4 rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] font-bold text-white w-full uppercase">Забронировать<span style="height: calc(100% - 2px); top:1px; right:1px;" class="text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-white"></i></span></button>
              </div>
            </div>

            <div class="flex flex-col content-center bg-white w-5/6 rounded-md px-8 pt-10 pb-8 gap-5 h-min">
              <div class="quicksand font-bold text-2xl">Войти</div>
              <div>
                <input type="email" class="rounded-md w-full" placeholder="Email">
              </div>
              <input type="password" class="rounded-md w-full" placeholder="Пароль">
              <div class="flex pt-4">
                  <button class="relative flex items-center justify-center py-4 rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] font-bold text-white w-full uppercase">Войти<span style="height: calc(100% - 2px); top:1px; right:1px;" class="text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-white"></i></span></button>
              </div>
              <div class="flex justify-between items-center pt-4">
                <div class="flex">Нет аккаунта на Toxin?</div>
                <div class="bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] rounded-full p-0.5">
                  <button class="flex items-center rounded-full border-none bg-white px-4 py-1 font-bold text-[#BC9CFF]">СОЗДАТЬ</button>
                </div>
              </div>
            </div>
          </div>

          <div class="flex flex-col gap-6 h-min items-center pt-5">

            <div class="flex flex-col content-center bg-white w-auto rounded-md ">
              <div id="indicators-carousel" class="relative" data-carousel="static">
                <!-- Carousel wrapper -->
                <div class="relative w-full h-40">
                  <!-- Item 1 -->
                  <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                    <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                  </div>
                  <!-- Item 2 -->
                  <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                    <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                  </div>
                  <!-- Item 3 -->
                  <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                    <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                  </div>
                  <!-- Item 4 -->
                  <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                    <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                  </div>
                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                  <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                  <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                  <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                  <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                  <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                </div>
                <!-- Slider controls -->
                <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                  <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <i class="ri-arrow-left-s-line"></i>
                    <span class="sr-only">Previous</span>
                  </span>
                </button>
                <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                  <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <i class="ri-arrow-right-s-line"></i>
                    <span class="sr-only">Next</span>
                  </span>
                </button>
              </div>
              <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                <div class="flex justify-between items-end">
                  <div class="flex items-end gap-1">
                    <span class="quicksand font-bold">№</span>
                    <span class="quicksand font-bold text-2xl">888</span>
                    <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                  </div>
                  <div class="flex montserrat gap-1">
                    <span class="font-bold text-slate-500">9 990₽</span>
                    <span class="font-normal text-slate-500">в сутки</span>
                  </div>
                </div>
                <div class="w-full h-px bg-slate-200">
                </div>
                <div class="flex justify-between items-center gap-8">
                  <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                    <span class="flex"><i class="ri-star-fill"></i></span>
                    <span class="flex"><i class="ri-star-fill"></i></span>
                    <span class="flex"><i class="ri-star-fill"></i></span>
                    <span class="flex"><i class="ri-star-fill"></i></span>
                    <span class="flex"><i class="ri-star-fill"></i></span>
                  </div>
                  <div class="flex montserrat gap-1">
                    <span class="font-bold text-slate-500">145</span>
                    <span class="font-normal text-slate-500">Отзывов</span>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    <% end %>
    """
  end

end
