defmodule PortfolioWeb.ComponentLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
      <div class="px-40 py-10 bg-black">
        <div class="space-y-10 md:grid md:grid-cols-3 space-y-10 md:grid md:gap-x-8 md:gap-y-10 md:space-y-0">
          <div class="bg-white flex flex-col gap-4 py-4">
            <div class="px-6">
              <label class="uppercase montserrat font-bold text-xs" for="">Text Field</label>
              <input type="email" class="rounded-md w-full" placeholder="Email">
            </div>

            <div class="px-6">
              <label class="uppercase montserrat font-bold text-xs" for="">Text Field</label>
              <input type="text" class="rounded-md w-full hover:text-[#1F2041] focus:text-[#1F2041]" placeholder="This is pretty awesome">
            </div>

            <div class="px-6 group inline-block relative">
              <div class="relative w-full">
                <button data-dropdown-toggle="dropdown" class="border border-black bg-white w-full rounded-md hover:rounded-t-md montserrat font-normal text-[#1f204180] px-4 py-2.5 text-center inline-flex items-center justify-between hover:rounded-t-md hover:rounded-b-none" type="text">Сколько гостей
                  <svg class="flex ml-2 w-4 h-4" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7">
                    </path>
                  </svg>
                </button>
                <!-- Dropdown menu -->
                <ul class="w-full absolute flex flex-row bg-white hidden uppercase montserrat font-bold text-xs border border-black group-hover:block ">
                  <div class="flex justify-between">
                    <div class="flex flex-col">
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Взрослые</a>
                      </li>
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Дети</a>
                      </li>
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Младенцы</a>
                      </li>
                    </div>
                    <div class="grid grid-cols-3 flex justify-around content-center items-start">
                      <div class="flex flex-col">
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                      </div>
                      <div class="flex flex-col">
                        <div class="py-2 px-4 font-bold text-2xs">0</div>
                        <div class="py-2 px-4 font-bold text-2xs">1</div>
                        <div class="py-2 px-4 font-bold text-2xs">2</div>
                      </div>
                      <div class="flex flex-col">
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                      </div>
                    </div>
                  </div>
                </ul>
              </div>
            </div>

            <div class="flex flex-col basis-1/2 px-6">
              <label class="uppercase montserrat font-bold text-xs" for="">Masked text field</label>
              <input class="rounded-md w-full" type="date" id="" name="trip-start" value="ГГГГ-ММ-ДД" min="1930-01-01" max="2022-12-31">
            </div>

            <div class="flex flex-row justify-center gap-4 w-full px-6">
              <div class="flex flex-col basis-1/2">
                <label class="uppercase montserrat font-bold text-xs" for="start">date Dropdown</label>
                <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
              </div>

              <div class="flex flex-col basis-1/2">
                <label class="uppercase montserrat font-bold text-xs" for="start">date Dropdown</label>
                <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
              </div>
            </div>

          </div>
          <div class="text-white border-2">привет

          </div>
          <div class="text-white border-2">привет

          </div>
        </div>
      </div>
    <% end %>
    """
  end

end
