defmodule PortfolioWeb.ServicesLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
      <div class="bg-white mt-10 px-40">
        <div class="grid grid-cols-4 gap-4">
          <div class="flex gap-6 flex-col">
            <div class="flex flex-row justify-center gap-4 w-full">
              <div class="flex flex-col basis-1/2">
                <label class="uppercase montserrat font-bold text-sm" for="start">Прибытие</label>
                <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
              </div>

              <div class="flex flex-col basis-1/2">
                <label class="uppercase montserrat font-bold text-sm" for="start">Выезд</label>
                <input class="rounded-md w-full" type="date" id="start" name="trip-start" value="ГГГГ-ММ-ДД" min="2000-01-01" max="2022-12-31">
              </div>
            </div>

            <div class="flex group inline-block relative w-full">
              <div class="relative w-full">
                <label class="uppercase montserrat font-bold text-sm" for="start">Гости</label>
                <button data-dropdown-toggle="dropdown" class="border border-black bg-white w-full rounded-md hover:rounded-t-md montserrat font-normal text-[#1f204180] px-4 py-2.5 text-center inline-flex items-center justify-between hover:rounded-t-md hover:rounded-b-none" type="text">Сколько гостей
                  <svg class="flex ml-2 w-4 h-4" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7">
                    </path>
                  </svg>
                </button>
                <!-- Dropdown menu -->
                <ul class="w-full absolute flex flex-row bg-white hidden uppercase montserrat font-bold text-xs border border-black group-hover:block ">
                  <div class="flex justify-between">
                    <div class="flex flex-col">
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Взрослые</a>
                      </li>
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Дети</a>
                      </li>
                      <li class="flex justify-between items-center">
                        <a class="bg-white flex hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap"
                          href="#"
                          >Младенцы</a>
                      </li>
                    </div>
                    <div class="grid grid-cols-3 flex justify-around content-center items-start">
                      <div class="flex flex-col">
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                      </div>
                      <div class="flex flex-col">
                        <div class="py-2 px-4 font-bold text-2xs">0</div>
                        <div class="py-2 px-4 font-bold text-2xs">1</div>
                        <div class="py-2 px-4 font-bold text-2xs">2</div>
                      </div>
                      <div class="flex flex-col">
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                        <button><i class="ri-add-circle-line flex py-2 px-4 font-bold text-2xs"></i></button>
                      </div>
                    </div>
                  </div>
                </ul>
              </div>
            </div>

            <div class="flex gap-2 flex-col">
              <label class="uppercase montserrat font-bold text-sm" for="start">Checkbox Buttons</label>
              <p class="flex items-center gap-4">
                <label class="flex gap-2 items-center">
                  <input class="flex justify-center" type="checkbox" name="title">Можно курить
                </label>
              </p>
              <p class="flex items-center gap-4">
                <label class="flex gap-2 items-center">
                  <input class="flex justify-center" type="checkbox" name="title">Можно с питомцами
                </label>
              </p>
              <p class="flex items-center gap-4">
                <label class="flex gap-2 items-center">
                  <input class="flex justify-center" type="checkbox" name="title">Можно пригласить гостей (до 10 человек)
                </label>
              </p>
            </div>

            <div class="flex gap-2 flex-col">
              <label class="uppercase montserrat font-bold text-sm" for="start">Доступность</label>
              <p class="flex items-center gap-4 font-bold">
                <label class="flex gap-2 items-center">
                  <input class="flex justify-center" type="checkbox" name="title">Широкий коридор
                </label>
              </p>
              <p class="flex items-center gap-4 font-bold">
                <label class="flex gap-2 items-center">
                  <input class="flex justify-center" type="checkbox" name="title">Помощник для инфалидов
                </label>
              </p>
            </div>


          </div>
          <div class="col-span-3 flex flex-col gap-4">
            <div class="quicksand font-bold text-2xl">
             Номера, которые мы для вас подобрали
            </div>
            <div class="flex grid grid-cols-3 gap-8 justify-items-stretch">
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex flex-col gap-6 h-min items-center pt-5">
                <div class="flex flex-col content-center bg-white w-auto rounded-md drop-shadow-lg ">
                  <div id="indicators-carousel" class="relative" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative w-full h-40">
                      <!-- Item 1 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 2 -->
                      <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-10" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 3 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                      <!-- Item 4 -->
                      <div class="hidden duration-700 ease-in-out absolute inset-0 transition-all transform" data-carousel-item>
                        <img src="/images/lux.png" class="absolute rounded-t-md block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2" alt="...">
                      </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                      <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                      <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 5" data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-left-s-line"></i>
                        <span class="sr-only">Previous</span>
                      </span>
                    </button>
                    <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                      <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <i class="ri-arrow-right-s-line"></i>
                        <span class="sr-only">Next</span>
                      </span>
                    </button>
                  </div>
                  <div class="px-8 pt-10 pb-8 gap-5 h-min" >
                    <div class="flex justify-between items-end">
                      <div class="flex items-end gap-1">
                        <span class="quicksand font-bold">№</span>
                        <span class="quicksand font-bold text-2xl">888</span>
                        <span class="montserrat font-bold text-[#BC9CFF]">люкс</span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">9 990₽</span>
                        <span class="font-normal text-slate-500">в сутки</span>
                      </div>
                    </div>
                    <div class="w-full h-px bg-slate-200">
                    </div>
                    <div class="flex justify-between items-center gap-8">
                      <div class="flex font-bold text-2xl gap-2 text-[#8BA4F9]">
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                        <span class="flex"><i class="ri-star-fill"></i></span>
                      </div>
                      <div class="flex montserrat gap-1">
                        <span class="font-bold text-slate-500">145</span>
                        <span class="font-normal text-slate-500">Отзывов</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>



            </div>
          </div>
        </div>
      </div>

      <div class="bg-white mt-10 px-40 pt-10">
        <div class="space-y-10 md:grid md:grid-cols-5 md:gap-x-8 md:gap-y-10 md:space-y-0 pb-20">
          <div class="flex flex-col gap-6">
            <div class="flex content-center">
              <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
            </div>
            <div class="montserrat font-medium text-[#1f204180]">Бронирование номеров в лучшем отеле 2019 года по версии ассоциации «Отельные взгляды»</div>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Навигация</a></li>
              <li><a href="">О нас</a></li>
              <li><a href="">Новости</a></li>
              <li><a href="">Служба поддержки</a></li>
              <li><a href="">Услуги</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">О нас</a></li>
              <li><a href="">О сервисе</a></li>
              <li><a href="">Наша команда</a></li>
              <li><a href="">Вакансии</a></li>
              <li><a href="">Инвесторы</a></li>
            </ul>
          </div>
          <div>
            <ul class= "flex flex-col justify-between gap-6 montserrat font-normal text-[#1f204180] hoverable">
              <li class="font-bold uppercase text-[#1f2041bf]"><a href="">Служба поддержки</a></li>
              <li><a href="">Соглашения</a></li>
              <li><a href="">Сообщества</a></li>
              <li><a href="">Связь с нами</a></li>
            </ul>
          </div>
          <div class="flex flex-col justify-start gap-6 montserrat font-normal text-[#1f204180] justify-start">
            <div class="font-bold uppercase text-[#1f2041bf]">Подписка</div>
            <div class="montserrat font-medium text-[#1f204180]">Получайте специальные предложения и новости сервиса</div>
            <div class="montserrat font-medium text-[#8BA4F9]">
              <div class="relative w-full ">
                <input type="email" class=" w-full rounded-md border-[#1f204140] text-[#8BA4F9]" placeholder="Email" />
                <span style="height: calc(100% - 2px); top:1px; right:1px;" class="bg-white text-base text-gray-500 absolute flex justify-center items-center px-4 rounded-r-md"><i class="ri-arrow-right-line text-2xl font-bold text-[#8BA4F9]"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="flex justify-between justify-items-center items-center h-16 border-t-2 border-[#1f20410d]">
          <div class="flex montserrat font-medium text-[#1f204180]">
          Copyright © 2018 Toxin UI Kit. All rights reserved.
          </div>
          <div class="flex gap-4 text-3xl text-[#8BA4F9]">
            <div><i class="ri-twitter-fill"></i></div>
            <div><i class="ri-facebook-box-fill"></i></div>
            <div><i class="ri-instagram-line"></i></div>
          </div>
        </div>
      </div>

    <% end %>
    """
  end

    # <div class="welcome-text">
    #   Добро пожаловать на мою страницу
    # </div>
end
