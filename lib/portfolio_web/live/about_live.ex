defmodule PortfolioWeb.AboutLive do
  use PortfolioWeb, :live_view
  def mount(_,_, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <%= live_component(PortfolioWeb.LiveComponents.Menu, id: :main_menu) do %>
      <div class="bg-gray-50">
        <div class="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:flex lg:items-center lg:justify-between lg:py-16 lg:px-8">
          <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            <span class="block">Ready to drive in?</span>
            <span class="block text-indigo-600">Start your free trial today.</span>
          </h2>
          <div class="mt-8 flex lg:mt-0 lg:flex-shrink-0">
            <div class="inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700">Get started</a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-indigo-600 hover:bg-indigo-50">Learn more</a>
            </div>
          </div>
        </div>
      </div>

      <div class="bg-indigo-600 rounded-md">
        <div class="flex flex-col justify-center items-center mx-auto max-w-7xl py-12 px-4 gap-6 sm:px-6 lg:flex lg:items-center lg:py-16 lg:px-8">
          <h2 class="flex flex-col items-center text-3xl font-bold tracking-tight text-white sm:text-4xl">
            <span>Boost your productivity.</span>
            <span>Start using Workflow today.</span>
          </h2>
          <div class="flex flex-col items-center text-xl tracking-tight text-white">
            <span> Ac euismod vel sit maecenas id pellentesque eu sed consectetur.</span>
            <span>Malesuada adipiscing sagittis vel nulla nec.</span>
          </div>
          <div class="ml-3 inline-flex rounded-md shadow">
            <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-indigo-600 hover:bg-indigo-50">Sing up for free</a>
          </div>
        </div>
      </div>

      <div class="bg-white rounded-md">
        <div class="gap-8 mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:flex lg:flex-col lg:items-center lg:justify-center lg:py-16 lg:px-8">
          <h2 class="flex flex-col items-center text-3xl font-bold tracking-tight sm:text-4xl">
            <span>Ready to drive in?</span>
            <span>Start your free trial today.</span>
          </h2>
          <div class="mt-8 flex lg:mt-0 lg:flex-shrink-0">
            <div class="inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700">Get started</a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-100 px-5 py-3 text-base font-medium text-indigo-600 hover:bg-indigo-50">Learn more</a>
            </div>
          </div>
        </div>
      </div>

      <div class="bg-gray-50 rounded-md">
        <div class="gap-8 flex flex-col justify-center mx-auto max-w-7xl py-12 px-4 gap-6 sm:px-6 lg:flex lg:py-16 lg:px-8">
          <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            <span class="block">Ready to drive in?</span>
            <span class="block text-indigo-600">Start your free trial today.</span>
          </h2>
          <div class="flex">
            <div class="inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700">Get started</a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow">
              <a href="#" class="inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-indigo-600 hover:bg-indigo-50">Learn more</a>
            </div>
          </div>
        </div>
      </div>

      <div class="bg-indigo-600 rounded-md">
        <div class="flex mx-auto max-w-7xl py-12 px-4 gap-6 sm:px-6 lg:flex lg:py-16 lg:px-8">
          <div class="flex flex-col justify-center mx-auto max-w-7xl py-12 px-4 gap-6 sm:px-6 lg:flex lg:py-16 lg:px-8">
            <h2 class="flex flex-col text-3xl font-bold tracking-tight text-white sm:text-4xl">
              <span>Boost your productivity.</span>
              <span>Start using Workflow today.</span>
            </h2>
            <div class="flex flex-col text-xl tracking-tight text-white ">
              Malesuada Ac euismod vel sit maecenas id pellentesque eu sed consectetur. Malesuada adipiscing sagittis vel nulla nec.
            </div>
            <div class="inline-flex rounded-md">
              <a href="#" class="inline-flex justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-indigo-600 hover:bg-indigo-50">Sing up for free</a>
            </div>
          </div>
          <div style="min-width: 320px;" class="w-full h-full flex justify-center items-center">
            <img width="" height="" class="rounded-md" src="/images/bitrix12.png" alt="2"/>
          </div>
        </div>
      </div>
    <% end %>
    """
  end

end
