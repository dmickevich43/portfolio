defmodule PortfolioWeb.LiveComponents.Menu do
  use PortfolioWeb, :live_component
  def render(assigns) do
    ~H"""
    <div class="w-full max-w-full h-3">
      <div class="fix_menu w-full max-w-full h-auto bg-white px-40 py-2 drop-shadow-md z-50">
        <div class="flex flex-row justify-between items-center gap-8 ">
          <div class="flex content-center justify-center">
            <a href="/">
              <img style="width: 105.69px; height: 40px;" class="" src="/images/Logo_toxin.png" alt="logo">
            </a>
          </div>
          <div class="flex flex-row gap-8">
            <div class="flex justify-end gap-8 text-sm">
              <ul class= "flex justify-between items-center gap-6 montserrat font-normal text-[#1f204180] topmenu">
                  <%= for link_data <- link_list(myself: @myself) do%>
                    <.link {link_data} />
                    <% end %>
              </ul>
            </div>
            <div class="flex flex-row justify-around content-center gap-4 py-2">
              <div class="bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] rounded-full p-0.5">
                <button class="flex items-center rounded-full border-none bg-white px-4 py-1 font-bold text-[#BC9CFF]"><a href="/entrance">ВОЙТИ</a></button>
              </div>
              <button class="flex items-center rounded-full bg-gradient-to-b from-[#bc9cff] via-[#a3a0fc] to-[#8ba4f9] px-4 py-1 font-bold text-white "><a href="/reg">ЗАРЕГИСТРИРОВАТЬСЯ</a></button>
            </div>
          </div>
        </div>
      </div>
      <div
        style="padding-top: 67px;"
        class="bg-white">
        <div
          style="height: calc(100vh - 67px);"
          class="overflow-hidden overflow-y-auto">
          <%= @inner_block.(%{}, assigns)%>
        </div>
      </div>
    </div>
    """
  end

  #
  # Компоненты
  #

  def link_list(myself: myself) do
    parse_link_to_interface(
      [
        [title: "О нас", to: "about_us"],
        [title: "Услуги", to: "services"],
        [title: "Вакансии", to: "contacts"],
        [title: "Новости", to: "skills"],
        [title: "Соглашения", to: "agreements"],
        [title: "Компоненты", to: "component"]
      ],
      myself
    )
  end

  def link(assigns) do
    ~H"""
    <li
      phx-target={@myself}
      phx-click={"move_to:" <> @to}
      class= "flex justify-between items-center gap-6 montserrat font-normal text-[#1f204180] topmenu cursor-pointer pb transition_up
      hover:font-bold
      hover:text-[#1f2041bf]">
      <span><%= @title %></span>
    </li>
    """
  end

  #
  # События
  #


  @doc """
    hum_path = humanaized path (человеческое название)
  """
  def handle_event("move_to:" <>hum_path, _, socket) do
    path =
      case hum_path do
         "about_us"  -> "/"
         "about"     -> "/about"
         "skills"    -> "/skills"
         "contacts"  -> "/contacts"
         "error"     -> "/error"
         "reg"       -> "/reg"
         "component" -> "/component"
         "services"  -> "/services"
         "agreements"-> "/agreements"
         _           -> "/error"

      end

    {:noreply, push_redirect(socket, to: path)}
  end


  #
  #Вспомогательные функции
  #

  def parse_link_to_interface(list, myself) do
    Enum.map(list, fn link_data ->
      to = Keyword.get(link_data, :to)

      link_data
      |>Keyword.put(:id, "menu_link_item_#{to}")
      |>Keyword.put(:myself, myself)
    end)
  end
end
